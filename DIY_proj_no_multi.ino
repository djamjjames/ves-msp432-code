#include <DHT.h>
//Determines whether we output to computer or not
#define DEBUGGING 0

#define TEMP_HUMI_PIN 24
#define TRIG_PIN_4 28
#define ECHO_PIN_4 27
#define TRIG_PIN_3 35
#define ECHO_PIN_3 36
#define TRIG_PIN_2 38
#define ECHO_PIN_2 37
#define TRIG_PIN_1 40
#define ECHO_PIN_1 39


int distance_1;
int distance_2;
int distance_3;
int distance_4;

int temperature;
int humidity;

int count=0;



DHT dht(TEMP_HUMI_PIN, DHT22);

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(TRIG_PIN_1, OUTPUT);
  pinMode(ECHO_PIN_1, INPUT);
  pinMode(TRIG_PIN_2, OUTPUT);
  pinMode(ECHO_PIN_2, INPUT);
  pinMode(TRIG_PIN_3, OUTPUT);
  pinMode(ECHO_PIN_3, INPUT);
  pinMode(TRIG_PIN_4, OUTPUT);
  pinMode(ECHO_PIN_4, INPUT);
  dht.begin();
}

void loop()
{
  // put your main code here, to run repeatedly:
   if(count%100==0) { //The temperature sensor is not as important 
     temperature=dht.readTemperature();
     humidity=dht.readHumidity();
   }
   distance_1=get_distance_1();
   distance_2=get_distance_2();
   distance_3=get_distance_3();
   distance_4=get_distance_4();
   
  Serial1.print('~');
  Serial1.write(highByte(distance_1));
  Serial1.write(lowByte(distance_1));
  Serial1.write(highByte(distance_2));
  Serial1.write(lowByte(distance_2));
  Serial1.write(highByte(distance_3));
  Serial1.write(lowByte(distance_3));
  Serial1.write(highByte(distance_4));
  Serial1.write(lowByte(distance_4));
  Serial1.write(highByte(temperature));
  Serial1.write(lowByte(temperature));
  Serial1.write(highByte(humidity));
  Serial1.write(lowByte(humidity));
  Serial1.print('~');
  
  #if DEBUGGING
  Serial.print("Time: ");
  Serial.print(millis());
  Serial.print("\n");
  
  
  Serial.print("Distance 1: ");
  Serial.print((distance_1));
  Serial.print("\n");

  Serial.print("Distance 2: ");
  Serial.print((distance_2));
  Serial.print("\n");

  Serial.print("Distance 3: ");
  Serial.print((distance_3));
  Serial.print("\n");

  Serial.print("Distance 4: ");
  Serial.print((distance_4));
  Serial.print("\n");
  
  Serial.print("Temp: ");
  Serial.print((temperature));
  Serial.print("\n");
  
  Serial.print("Humidity: ");
  Serial.print((humidity));
  Serial.print("\n");
  #endif
  
  count++;
}

int get_distance_1() {
  unsigned int duration;
  unsigned int distance;
  //Send initiate pulse:
  digitalWrite(TRIG_PIN_1, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_1, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_1, LOW);
  duration=pulseIn(ECHO_PIN_1, HIGH, 1000);
  
  if(duration>=30000) {
     distance=1000;
  }
  else if(duration==0) {
     distance=-1; 
  }
  else {
    distance=duration/29/2;
  }
  
  return distance;
}

int get_distance_2() {
  unsigned int duration;
  unsigned int distance;
  //Send initiate pulse:
  digitalWrite(TRIG_PIN_2, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_2, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_2, LOW);
  duration=pulseIn(ECHO_PIN_2, HIGH, 1000);
  
  if(duration>=30000) {
     distance=1000;
  }
  else if(duration==0) {
     distance=-1; 
  }
  else {
    distance=duration/29/2;
  }
  
  return distance;
}

int get_distance_3() {
  unsigned int duration;
  unsigned int distance;
  //Send initiate pulse:
  digitalWrite(TRIG_PIN_3, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_3, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_3, LOW);
  duration=pulseIn(ECHO_PIN_3, HIGH, 1000);
 
  if(duration>=30000) {
     distance=1000;
  }
  else if(duration==0) {
     distance=-1; 
  }
  else {
    distance=duration/29/2;
  }
  
  return distance;
}

int get_distance_4() {
  unsigned int duration;
  unsigned int distance;
  //Send initiate pulse:
  digitalWrite(TRIG_PIN_4, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN_4, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN_4, LOW);
  duration=pulseIn(ECHO_PIN_4, HIGH, 1000);
  
  if(duration>=30000) {
     distance=1000;
  }
  else if(duration==0) {
     distance=-1; 
  }
  else {
    distance=duration/29/2;
  }
  
  return distance;
}
